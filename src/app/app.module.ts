import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductosComponent } from './paginas/productos/productos.component';

import { SwiperModule } from 'swiper/angular';
import { PaginasModule } from './paginas/paginas.module';
import { MenuComponent } from './shared/menu/menu.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './paginas/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { DialogoConfirmacionComponent } from './paginas/dialogo-confirmacion/dialogo-confirmacion.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    MenuComponent,
    FooterComponent,
    LoginComponent,
    DialogoConfirmacionComponent,// <--- Aquí
    
  ],
  imports: [
    SwiperModule,
    BrowserModule,
    AppRoutingModule,
    PaginasModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule, // <--- Aquí
    BrowserAnimationsModule, // <--- Aquí
    MatButtonModule, // <--- Aquí
    
  ],
  providers: [],
  bootstrap: [AppComponent],entryComponents: [
    DialogoConfirmacionComponent// <--- Aquí
  ]
})
export class AppModule { }
