import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  public postLogin(body: any) {
    const url = 'http://localhost:7000/login';
    return this.http.post(url, body);
  }
}
