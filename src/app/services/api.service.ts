import { Injectable } from '@angular/core';
import { LoginI } from '../model/login.interfaces';
import { ResponseI } from '../model/response.interface';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url:string="http://localhost:7000/login"
  constructor(private http:HttpClient) { }

  loginByRol(form:LoginI):Observable<ResponseI>{
    let direccion =this.url;    
    return this.http.post<ResponseI>(direccion,form);
  }
}
