import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProvedorService {

  constructor(private http:HttpClient) { }

  public getProveedores(){
    const url=`http://localhost:7000/proveedores`
    return this.http.get(url)
  }


  public createProveedor(body:any){
    const url=`http://localhost:7000/proveedor`
    return this.http.post(url,body)
  }

  public deleteProveedor(body:any){
    const url=`http://localhost:7000/proveedor/`
    return this.http.put(url,body)
  }


  public updateProveedor(body:any){
    const url=`http://localhost:7000/proveedor`
    return this.http.put(url,body)
  }

}
