import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service'; 
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { LoginI } from 'src/app/model/login.interfaces';
import { RolesService } from 'src/app/services/roles.service';
import { ResponseI } from 'src/app/model/response.interface';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public form!: FormGroup;
  public loggedIn: boolean = false;
  private rol_email: string = '';
  private rol_password: string = '';
  private rol_id:number=0;
  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,    
    private router:Router,
    private api:ApiService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      rol_email: new FormControl(this.rol_email, [Validators.required]),
      rol_password: new FormControl(this.rol_password, [Validators.required]),
    });
  }
  errorStatus:boolean=false;
  
  onLogin(form:LoginI){
    this.api.loginByRol(form).subscribe(data=>{        
        let dataResponse:ResponseI=data;        
        if(dataResponse.accesstoken=="true"){
          localStorage.setItem("token",dataResponse.result.token);
          this.router.navigate(['productos'])
        }else{
          this.errorStatus=true
        }     
      })
  }

  login() {
    try {
      this.loginService
        .postLogin({
          rol_email: this.form.value.rol_email,
          rol_password: this.form.value.rol_password,
        })
        .subscribe((res: any) => {       
          let dataResponse:ResponseI=res;        
          if(dataResponse.accesstoken){
            localStorage.setItem("token",dataResponse.accesstoken);
            this.router.navigate(['productos'])
            console.log('Mensaje' , res);
            this.loggedIn = true;
          }else{
            this.errorStatus=true
            console.log('Error' , res);
          }     
        });
      
    } catch (error) {
      console.error(error);
    }
  }

  get isLoggedIn(): boolean {
    return this.loggedIn;
  }
}
