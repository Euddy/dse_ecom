export interface ModuleCartCab {
    cart_cab_id:       number;
    user_id:           number;
    cart_cab_date:     Date;
    cart_cab_iva:      number;
    cart_cab_discount: number;
    cart_cab_subtotal: number;
    cart_cab_total:    number;
    cart_cab_state:    boolean;
}
