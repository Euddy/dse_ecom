export interface ModuleProveedor {
    prov_id:        number;
    prov_cedula:    string;
    prov_nombre:    string;
    prov_direccion: string;
    prov_telefono:  string;
    prov_state:     boolean;
}
