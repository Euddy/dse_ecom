import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudProvedorComponent } from './crud-provedor.component';

describe('CrudProvedorComponent', () => {
  let component: CrudProvedorComponent;
  let fixture: ComponentFixture<CrudProvedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudProvedorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudProvedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
