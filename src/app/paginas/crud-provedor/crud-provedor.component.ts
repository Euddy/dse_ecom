import { Component, OnInit } from '@angular/core';
//import Swiper from 'swiper';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProvedorService } from '../../services/provedor.service';
import { ModuleProveedor } from '../interfaces/model.proveedores';

//IMPORTANDO LAS LIBRERIAS PARA LOS CONFIRM DIALOG
import { MatDialog } from "@angular/material/dialog";
import { DialogoConfirmacionComponent } from '../dialogo-confirmacion/dialogo-confirmacion.component';


@Component({
  selector: 'app-crud-provedor',
  templateUrl: './crud-provedor.component.html',
  styleUrls: ['./crud-provedor.component.css']
})
export class CrudProvedorComponent implements OnInit {

  public form!: FormGroup;
  //crud-productos.component.ts
  public informacionProvedores = {
    prov_id: -1,
    prov_cedula: '',
    prov_nombre: '',
    prov_direccion: '',
    prov_telefono: '',
    prov_state: true
  };

  constructor(private proveedorService: ProvedorService, private formbuilder: FormBuilder, public dialogo: MatDialog) { }

  proveedor: ModuleProveedor[] = [];


  ngOnInit(): void {
    this.cargarProveedores()
    this.form = this.formbuilder.group({
      //txtProv_id: [''],
      txtCedula: [''],
      txtNombre: [''],
      txtDireccion: [''],
      txtTelefono: [''],
      txtState: ['']
    });
   
  }
  public cargarProveedores() {
    this.proveedorService.getProveedores().subscribe(
      (proveedor: any) => {
        this.proveedor = proveedor;
        console.log(this.proveedor);
      },
      (error) => console.log(error)
    );
  }
  
  public crearProveedor() {
    this.proveedorService.createProveedor({
      prov_cedula: this.form.value.txtCedula,
      prov_nombre: this.form.value.txtNombre,
      prov_direccion: this.form.value.txtDireccion,
      prov_telefono: this.form.value.txtTelefono,
      prov_state: this.form.value.txtState,

    })
      .subscribe((res) => {
        console.log('Proveedor Creado Exitosamente');
        this.cargarProveedores();
      });
  }

  public eliminarProveedor(prov_id: any) {
    console.log(prov_id);
    this.proveedorService.deleteProveedor({
        prov_id: prov_id,
        prov_state: false})
      .subscribe((res) => {
        console.log('Provedor Eliminado');
        this.cargarProveedores();
      });
  }
  public actualizarProveedor(prov_id: any) {
    this.proveedorService.updateProveedor({
      prov_id: prov_id,
      prov_cedula: this.form.value.txtCedula,
      prov_nombre: this.form.value.txtNombre,
      prov_direccion: this.form.value.txtDireccion,
      prov_telefono: this.form.value.txtTelefono,
      prov_state: this.form.value.txtState
    }).subscribe(res => {
      console.log('Proveedor actualizado');
      this.cargarProveedores()
    })
  }
  public infoUpdateProveedor(prov_id: any, prov_cedula: any, prov_nombre: any, prov_direccion: any, prov_telefono: any, prov_state: any
  ) {
    this.informacionProvedores.prov_id = prov_id;
    this.informacionProvedores.prov_cedula = prov_cedula;
    this.informacionProvedores.prov_nombre = prov_nombre;
    this.informacionProvedores.prov_direccion = prov_direccion;
    this.informacionProvedores.prov_telefono = prov_telefono;
    this.informacionProvedores.prov_state = prov_state;
  }

  public activeModal() {
    var modal = document.getElementById('myModal');
    modal?.classList.add('modal_show');
  }

  public inactiveModal() {
    var modal = document.getElementById('myModal');
    modal?.classList.remove('modal_show');
  }

////Metodo para el mensaje de confirmacion
mostrarDialogo(prov_id:any): void {
  this.dialogo
    .open(DialogoConfirmacionComponent, {
      data: `¿Esta seguro de eliminar?`
    })
    .afterClosed()
    .subscribe((confirmado: Boolean) => {
      if (confirmado) {
        this.eliminarProveedor(prov_id)
        alert("¡Elemento eliminado correctamente!");
      } else {
        alert("Elemento no eliminado");
      }
    });
}

////Metodo para el mensaje de confirmacion de actualizacion
mostrarDialogoAct(prov_id:any): void {
  this.dialogo
    .open(DialogoConfirmacionComponent, {
      data: `¿Esta seguro de actualizar?`
    })
    .afterClosed()
    .subscribe((confirmado: Boolean) => {
      if (confirmado) {
        this.actualizarProveedor(prov_id)
        alert("¡Elemento actualizado correctamente!");
      } else {
        alert("Elemento no actualizado");
      }
    });
}

}
