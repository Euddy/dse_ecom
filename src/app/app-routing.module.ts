import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { ProductosComponent } from './paginas/productos/productos.component';
import { CrudClientesComponent } from './paginas/crud-clientes/crud-clientes.component';
import { CrudProductosComponent } from './paginas/crud-productos/crud-productos.component';
import { CrudRolesComponent } from './paginas/crud-roles/crud-roles.component';
import { CrudPorductosCategoriaComponent } from './paginas/crud-porductos-categoria/crud-porductos-categoria.component';
import { CurdoCarritoComponent } from './paginas/curdo-carrito/curdo-carrito.component';
import { PizzaComponent } from './paginas/pizza/pizza.component';
import { CrudSeccionComponent } from './paginas/crud-seccion/crud-seccion.component';
import { ListaCarritoComponent } from './paginas/lista-carrito/lista-carrito.component';
import { DetalleVentasComponent } from './paginas/detalle-ventas/detalle-ventas.component';
import { LoginComponent } from "./paginas/login/login.component";
import { CrudProvedorComponent } from "./paginas/crud-provedor/crud-provedor.component";
import  * as Material from "@angular/core"

const routes:Routes=[

    {
      path: '' ,
      component:ProductosComponent,
      pathMatch: 'full'
    },
    {
      path:'clientes',
      component: CrudClientesComponent
    }, 
    {
      path: 'proveedor',
      component:CrudProvedorComponent
  },
    {
      path:'login',
      component: LoginComponent
    },
    {
        path: 'productos',
        component:CrudProductosComponent
    },
    {
        path:'roles',
        component:CrudRolesComponent
    },
    {
        path:'carrito',
        component:CurdoCarritoComponent
    },
    {
        path:'productos-categoria',
        component:CrudPorductosCategoriaComponent
    },
    {
      path:'pizza',
      component: PizzaComponent

    },
    {
      path: 'seccion',
      component: CrudSeccionComponent
    },
    {
      path: 'ventas',
      component: ListaCarritoComponent
    },{
      path:'detalle/:cart_cab_id/:cart_cab_date/:cart_cab_total',
      component:DetalleVentasComponent
    },
    {
        path: '**',
        redirectTo: ''
    }


  ];

  @NgModule({
      imports:[
        RouterModule.forRoot(routes)
      ],
      exports:[
        RouterModule
      ]
  })

  export class AppRoutingModule {}